<html>

<head>
    <link href="{{ url('css/bootstrap.min.css') }}" type="text/css" rel="stylesheet" />
    <script src="{{ mix('js/app.js') }}" type="text/javascript" defer></script>
</head>

<body style='background-color: white'>
    <div id="app">
    </div>
</body>

</html>